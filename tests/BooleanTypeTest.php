<?php


namespace judahnator\Schema\Tests;

use InvalidArgumentException;
use judahnator\Schema\BooleanType;
use judahnator\Schema\Builder;

final class BooleanTypeTest extends TypeTestCase
{
    public function testCreatingValue(): void
    {
        $schema = Builder::boolean();
        $this->assertInstanceOf(Builder::class, $schema);
        $this->assertEquals(Builder::TYPE_BOOLEAN, $schema->getType());

        $structure = $schema->create(false);
        $this->assertInstanceOf(BooleanType::class, $structure);
        $this->assertFalse($structure->getValue());
    }

    public function testCreatingInvalidValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The input for this schema type must be boolean.');
        Builder::boolean()->create('a');
    }

    public function testFakingValue(): void
    {
        $this->assertFalse(Builder::boolean()->fake()->getValue());
    }
}
