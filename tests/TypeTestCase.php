<?php


namespace judahnator\Schema\Tests;

use PHPUnit\Framework\TestCase;

abstract class TypeTestCase extends TestCase
{
    abstract public function testCreatingValue(): void;

    abstract public function testCreatingInvalidValue(): void;

    abstract public function testFakingValue(): void;
}
