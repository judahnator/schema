<?php


namespace judahnator\Schema\Tests;

use InvalidArgumentException;
use judahnator\Schema\Builder;
use judahnator\Schema\NumberType;

final class NumberTypeTest extends TypeTestCase
{
    public function testCreatingValue(): void
    {
        $schema = Builder::number();
        $this->assertEquals(Builder::TYPE_NUMBER, $schema->getType());

        $structure = $schema->create(21);
        $this->assertInstanceOf(NumberType::class, $structure);
        $this->assertEquals(21, $structure->getValue());
    }

    public function testCreatingInvalidValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The input for this schema type must be a number.');
        Builder::number()->create('asdf');
    }

    public function testFakingValue(): void
    {
        $this->assertEquals(0, Builder::number()->fake()->getValue());
    }
}
