<?php


namespace judahnator\Schema\Tests;

use judahnator\Schema\Builder;
use judahnator\Schema\StringType;

final class StringTypeTest extends TypeTestCase
{
    public function testCreatingValue(): void
    {
        $schema = Builder::string();
        $this->assertInstanceOf(Builder::class, $schema);
        $this->assertEquals(Builder::TYPE_STRING, $schema->getType());

        $structure = $schema->create('foobar');
        $this->assertInstanceOf(StringType::class, $structure);
        $this->assertEquals('foobar', $structure->getValue());
    }

    public function testCreatingInvalidValue(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The input for this schema type must be a string.');
        Builder::string()->create([]);
    }

    public function testFakingValue(): void
    {
        $this->assertEquals('', Builder::string()->fake()->getValue());
    }
}
