<?php


namespace judahnator\Schema\Tests;

use InvalidArgumentException;
use judahnator\Schema\Builder;
use judahnator\Schema\ObjectType;
use LogicException;
use stdClass;

final class ObjectTypeTest extends TypeTestCase
{
    public function testCreatingValue(): void
    {
        // Simple object
        $schema = Builder::object([
            'key1' => Builder::string(),
            'key2' => Builder::string()
        ]);
        $this->assertEquals(Builder::TYPE_OBJECT, $schema->getType());

        $input = (object)['key1' => 'foo', 'key2' => 'bar'];
        $structure = $schema->create($input);
        $this->assertInstanceOf(ObjectType::class, $structure);
        $this->assertEquals($input, $structure->getValue());

        // Complex object
        $schema = Builder::array(
            Builder::object([
                'stringKey' => Builder::string(),
                'arrayKey' => Builder::array(Builder::string()),
                'objectKey' => Builder::object([
                    'nested' => Builder::string()
                ]),
                'numberKey' => Builder::number(),
                'boolKey' => Builder::boolean(),
                'nullableKey' => Builder::string()->nullable()
            ])
        );

        $input = [
            (object)[
                'stringKey' => 'el1',
                'arrayKey' => [
                    'nestedElArr1-1',
                    'nestedElArr1-2'
                ],
                'objectKey' => (object)[
                    'nested' => 'nestedEl1ObjString'
                ],
                'numberKey' => 21,
                'boolKey' => true,
                'nullableKey' => null
            ],
            (object)[
                'stringKey' => 'el2',
                'arrayKey' => [
                    'nestedElArr2-1',
                    'nestedElArr2-2'
                ],
                'objectKey' => (object)[
                    'nested' => 'nestedEl2ObjString'
                ],
                'numberKey' => 42.24,
                'boolKey' => false,
                'nullableKey' => 'asdf'
            ]
        ];
        $structure = $schema->create($input);
        $this->assertEquals($input, $structure->getValue());
        $this->assertEquals(json_encode($input), json_encode($structure));
    }

    public function testCreatingInvalidValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The input for this schema type must be an object.');
        Builder::object(['foo' => Builder::string()])->create('asdf');
    }

    public function testCreatingWithMissingData(): void
    {
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Missing value "foo" for schema creation.');
        Builder::object(['foo' => Builder::string()])->create(new stdClass());
    }

    public function testCreatingWithOptionalData(): void
    {
        $schema = Builder::object([
            'requiredKey' => Builder::string(),
            'nullableKey' => Builder::string()->nullable(),
            'optionalKey' => Builder::string()->optional()
        ]);

        $this->assertEquals(
            (object)[
                'requiredKey' => 'required',
                'nullableKey' => 'present',
                'optionalKey' => 'present'
            ],
            $schema->create((object)[
                'requiredKey' => 'required',
                'nullableKey' => 'present',
                'optionalKey' => 'present'
            ])->getValue()
        );

        $this->assertEquals(
            (object)[
                'requiredKey' => 'required',
                'nullableKey' => null
            ],
            $schema->create((object)[
                'requiredKey' => 'required',
                'nullableKey' => null
            ])->getValue()
        );

        $this->assertEquals(
            (object)[
                'requiredKey' => '',
                'nullableKey' => '',
                'optionalKey' => ''
            ],
            $schema->fake()->getValue()
        );

        $faked = $schema->fake(true, true)->getValue();
        $this->assertEquals(
            (object)[
                'requiredKey' => '',
                'nullableKey' => null
            ],
            $faked
        );
    }

    public function testFakingValue(): void
    {
        $this->assertEquals(
            (object)[
                'string' => '',
                'array' => [0],
                'object' => (object)[
                    'nested' => false
                ]
            ],
            Builder::object([
                'string' => Builder::string(),
                'array' => Builder::array(Builder::number()),
                'object' => Builder::object([
                    'nested' => Builder::boolean()
                ])
            ])->fake()->getValue()
        );
    }
}
