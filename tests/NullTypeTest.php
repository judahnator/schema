<?php


namespace judahnator\Schema\Tests;

use judahnator\Schema\Builder;
use judahnator\Schema\NullType;

final class NullTypeTest extends TypeTestCase
{
    public function testCreatingValue(): void
    {
        $schema = Builder::boolean()->nullable()->create();
        $this->assertInstanceOf(NullType::class, $schema);
        $this->assertNull($schema->getValue());
    }

    public function testCreatingInvalidValue(): void
    {
        // we cannot really create an invalid null value, so lets make sure we dont get one when we dont need one
        $this->assertTrue(Builder::boolean()->nullable()->create(true)->getValue());
    }

    public function testFakingValue(): void
    {
        $this->assertNull(Builder::number()->nullable()->fake(true)->getValue());
    }
}
