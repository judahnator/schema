<?php

declare(strict_types=1);

namespace judahnator\Schema\Tests;

use ArrayIterator;
use InvalidArgumentException;
use judahnator\Schema\Builder;
use judahnator\Schema\MapType;

final class MapTypeTest extends TypeTestCase
{
    public function testCreatingValue(): void
    {
        $schema = Builder::map(Builder::string(), Builder::string());
        $this->assertEquals(Builder::TYPE_MAP, $schema->getType());

        // simple - array input
        $input1 = ['foo' => 'bar', 'bing' => 'baz'];
        $structure1 = $schema->create($input1);
        $this->assertInstanceOf(MapType::class, $structure1);
        $this->assertEquals($input1, $structure1->getValue());

        // complex
        $schema = Builder::map(
            Builder::string(),
            Builder::map(
                Builder::number(),
                Builder::string()
            )
        );
        $input2 = [
            'foo' => [12 => 'asdf'],
            'bar' => [34 => 'fdsa'],
        ];
        $structure2 = $schema->create($input2);
        $this->assertEquals($input2, $structure2->getValue());
        $this->assertEquals(json_encode($input2), json_encode($structure2));

        // simple - not array but traversable
        $input3 = new ArrayIterator($input1);
        $structure3 = Builder::map(Builder::string(), Builder::string())->create($input3);
        $this->assertEquals($input1, $structure3->getValue());
    }

    public function testCreatingInvalidValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The input for this schema type must be an array or traversable.');
        Builder::map(Builder::string(), Builder::string())->create('asdf');
    }

    public function testFakingValue(): void
    {
        $this->assertEquals(
            [
                0 => [false]
            ],
            Builder::map(
                Builder::number(),
                Builder::array(Builder::boolean())
            )->fake()->getValue()
        );
    }
}
