<?php


namespace judahnator\Schema\Tests;

use judahnator\Schema\Builder;
use PHPUnit\Framework\TestCase;

final class BuilderTest extends TestCase
{
    public function testBuilderDebuginfo(): void
    {
        // simple one-level
        $this->assertEquals(
            ['type' => 'string',],
            Builder::string()->__debugInfo()
        );

        // arrays with nested type
        $this->assertEquals(
            [
                'type' => 'array',
                'structure' => [
                    [
                        'type' => 'string',
                        'nullable' => 1
                    ]
                ]
            ],
            Builder::array(Builder::string()->nullable())->__debugInfo()
        );

        // more complex objects
        $this->assertEquals(
            [
                'type' => 'object',
                'structure' => [
                    'k1' => [
                        'type' => 'array',
                        'optional' => 1,
                        'structure' => [
                            [
                                'type' => 'number'
                            ]
                        ]
                    ],
                    'k2' => [
                        'type' => 'boolean',
                        'nullable' => 1
                    ]
                ]
            ],
            Builder::object([
                'k1' => Builder::array(Builder::number())->optional(),
                'k2' => Builder::boolean()->nullable()
            ])->__debugInfo()
        );
    }
}
