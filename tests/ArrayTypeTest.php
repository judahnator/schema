<?php


namespace judahnator\Schema\Tests;

use InvalidArgumentException;
use judahnator\Schema\ArrayType;
use judahnator\Schema\Builder;

final class ArrayTypeTest extends TypeTestCase
{
    public function testCreatingValue(): void
    {
        $schema = Builder::array(Builder::string());
        $this->assertEquals(Builder::TYPE_ARRAY, $schema->getType());

        $structure = $schema->create(['foo', 'bar']);
        $this->assertInstanceOf(ArrayType::class, $structure);
        $this->assertEquals(['foo', 'bar'], $structure->getValue());
    }

    public function testCreatingInvalidValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The input for this schema type must be an array or traversable.');
        Builder::array(Builder::number())->create('');
    }

    public function testFakingValue(): void
    {
        $this->assertEquals([''], Builder::array(Builder::string())->fake()->getValue());
    }
}
