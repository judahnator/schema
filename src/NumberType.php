<?php


namespace judahnator\Schema;

use judahnator\Schema\Support\Structure;

final class NumberType extends Structure
{
    public function __construct(float $value)
    {
        parent::__construct($value);
    }

    /**
     * Returns the raw value of the given object.
     */
    public function getValue(): float
    {
        return $this->value;
    }
}
