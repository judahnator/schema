<?php

declare(strict_types=1);

namespace judahnator\Schema;

use Generator;
use InvalidArgumentException;
use judahnator\Schema\Support\Structure;
use LogicException;
use RuntimeException;
use stdClass;
use Traversable;

final class Builder
{
    public const TYPE_ARRAY = 0;
    public const TYPE_BOOLEAN = 1;
    public const TYPE_MAP = 2;
    public const TYPE_NUMBER = 3;
    public const TYPE_OBJECT = 4;
    public const TYPE_STRING = 5;
    public const TYPES = [
        self::TYPE_ARRAY => 'array',
        self::TYPE_BOOLEAN => 'boolean',
        self::TYPE_MAP => 'map',
        self::TYPE_NUMBER => 'number',
        self::TYPE_OBJECT => 'object',
        self::TYPE_STRING => 'string'
    ];

    private $map = [];
    private $nullable = false;
    private $optional = false;
    private $type = null;

    public function __debugInfo(): array
    {
        return iterator_to_array((function (): Generator {
            $thisType = $this->getType();
            yield 'type' => self::TYPES[$thisType];
            $this->nullable && yield 'nullable' => 1;
            $this->optional && yield 'optional' => 1;
            if ($thisType === self::TYPE_ARRAY || $thisType === self::TYPE_OBJECT) {
                yield 'structure' => array_map(
                    function (Builder $nestedBuilder): array {
                        return $nestedBuilder->__debugInfo();
                    },
                    $this->map
                );
            }
        })());
    }

    /**
     * Creates an array schema.
     *
     * @param Builder $expectedType
     * @return Builder
     */
    public static function array(Builder $expectedType): self
    {
        $schema = (new self)->setType(self::TYPE_ARRAY);
        $schema->map = [$expectedType];
        return $schema;
    }

    /**
     * Creates a boolean schema.
     *
     * @return Builder
     */
    public static function boolean(): self
    {
        return (new self)->setType(self::TYPE_BOOLEAN);
    }

    /**
     * Creates a map-type schema.
     *
     * @param Builder $keyType
     * @param Builder $valueType
     * @return static
     */
    public static function map(self $keyType, self $valueType): self
    {
        if (!in_array($keyType->getType(), [self::TYPE_NUMBER, self::TYPE_STRING])) {
            throw new InvalidArgumentException('An object map key type must either a number or string.');
        }
        $schema = (new self())->setType(self::TYPE_MAP);
        $schema->map = [$keyType, $valueType];
        return $schema;
    }

    /**
     * Creates a number schema.
     *
     * @return Builder
     */
    public static function number(): self
    {
        return (new self)->setType(self::TYPE_NUMBER);
    }

    /**
     * Creates an object schema.
     *
     * The object requires a map of string key => Builder elements,
     * eg: 'myKey' => Builder::string();
     *
     * Object schemas will respect their nested elements 'nullable' and 'optional' properties.
     *
     * @param array<string, Builder> $objectMap
     * @return Builder
     */
    public static function object(array $objectMap): self
    {
        $schema = (new self)->setType(self::TYPE_OBJECT);
        foreach ($objectMap as $key => $item) {
            if (!is_string($key)) {
                throw new InvalidArgumentException('Object maps must be keyed with strings.');
            }
            if (!$item instanceof Builder) {
                throw new RuntimeException('Invalid object type provided.');
            }
            $schema->map[$key] = $item;
        }
        return $schema;
    }

    /**
     * Creates a string schema.
     *
     * @return Builder
     */
    public static function string(): self
    {
        return (new self)->setType(self::TYPE_STRING);
    }

    /**
     * Uses this schema to generate the appropriate data structure, validating the data in the process.
     *
     * @param mixed|null $data
     * @return Structure
     */
    public function create($data = null): Structure
    {
        if (is_null($data)) {
            if (!$this->nullable) {
                throw new InvalidArgumentException('Cannot use a null value for a non nullable schema.');
            }
            return new NullType();
        }

        switch ($this->type) {

            case self::TYPE_ARRAY:
                if (!is_array($data) && !$data instanceof Traversable) {
                    throw new InvalidArgumentException('The input for this schema type must be an array or traversable.');
                }
                /** @var Builder $expectedType */
                $expectedType = reset($this->map);
                return new ArrayType(...(function () use ($data, $expectedType): Generator {
                    foreach ($data as $datum) {
                        yield $expectedType->create($datum);
                    }
                })());

            case self::TYPE_BOOLEAN:
                if (!is_bool($data)) {
                    throw new InvalidArgumentException('The input for this schema type must be boolean.');
                }
                return new BooleanType($data);

            case self::TYPE_MAP:
                if (!is_array($data) && !$data instanceof Traversable) {
                    throw new InvalidArgumentException('The input for this schema type must be an array or traversable.');
                }
                /** @var Builder $keyType */
                /** @var Builder $valueType */
                [$keyType, $valueType] = $this->map;
                $input = [];
                foreach ($data as $key => $value) {
                    $input[$keyType->create($key)->getValue()] = $valueType->create($value);
                }
                return new MapType($input);

            case self::TYPE_NUMBER:
                if (!is_numeric($data)) {
                    throw new InvalidArgumentException('The input for this schema type must be a number.');
                }
                return new NumberType((float)$data);

            case self::TYPE_OBJECT:
                if (!is_object($data)) {
                    throw new InvalidArgumentException('The input for this schema type must be an object.');
                }
                $object = new stdClass();
                foreach ($this->map as $key => $expected) {
                    /** @var Builder $expected */

                    $provided = $data->{$key} ?? null;

                    if (is_null($provided) && !$expected->nullable) {
                        if ($expected->optional) {
                            // This item was optional so no need to throw an exception, just omit it
                            continue;
                        }

                        throw new LogicException("Missing value \"{$key}\" for schema creation.");
                    }

                    $object->{$key} = $expected->create($provided);
                }
                return new ObjectType($object);

            case self::TYPE_STRING:
                if (!is_string($data)) {
                    throw new InvalidArgumentException('The input for this schema type must be a string.');
                }
                return new StringType($data);

            default:
                throw new RuntimeException('Your mother was a hamster, and your father smelt of elderberries!');

        }
    }

    /**
     * Generates an example data structure with this schema.
     * Optionally making nullable attributes null and excluding optional ones.
     *
     * @param bool $makeNullableNull
     * @param bool $excludeOptional
     * @return Structure
     */
    public function fake(bool $makeNullableNull = false, $excludeOptional = false): Structure
    {
        if ($makeNullableNull && $this->nullable) {
            return new NullType();
        }

        switch ($this->type) {

            case self::TYPE_ARRAY:
                /** @var Builder $expectedType */
                $expectedType = reset($this->map);
                return new ArrayType($expectedType->fake());

            case self::TYPE_BOOLEAN:
                return new BooleanType(false);

            case self::TYPE_MAP:
                return new MapType([
                    $this->map[0]->fake()->getValue() => $this->map[1]->fake($makeNullableNull, $excludeOptional)
                ]);

            case self::TYPE_NUMBER:
                return new NumberType(0);

            case self::TYPE_OBJECT:
                $object = new stdClass();
                foreach ($this->map as $key => $expected) {
                    /** @var Builder $expected */
                    if ($excludeOptional && $expected->optional) {
                        continue;
                    }
                    $object->{$key} = $expected->fake();
                }
                return new ObjectType($object);

            case self::TYPE_STRING:
                return new StringType('');

            default:
                throw new RuntimeException('Your mother was a hamster, and your father smelt of elderberries!');

        }
    }

    /**
     * Gets the current schema type;
     *
     * @return int
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * Mark this schema item as nullable.
     *
     * @return Builder
     */
    public function nullable(): self
    {
        $this->nullable = true;
        return $this;
    }

    /**
     * Mark this schema item as optional.
     *
     * @return Builder
     */
    public function optional(): self
    {
        $this->optional = true;
        return $this;
    }

    /**
     * Sets the type of this schema element.
     *
     * @param int|string $type
     * @return Builder
     */
    protected function setType($type): self
    {
        if ($this->type) {
            throw new LogicException('Can not change the type of a schema.');
        }

        if (is_int($type) && array_key_exists($type, self::TYPES)) {
            $this->type = $type;
        } elseif (is_string($type) && in_array($type, self::TYPES)) {
            $this->type = array_search($type, self::TYPES);
        } else {
            throw new InvalidArgumentException('Unknown schema type provided.');
        }
        return $this;
    }
}
