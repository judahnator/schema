<?php

declare(strict_types=1);

namespace judahnator\Schema;

use judahnator\Schema\Support\Structure;

final class MapType extends Structure
{
    public function __construct($values)
    {
        parent::__construct($values);
    }

    /**
     * @inheritDoc
     */
    public function getValue(): array
    {
        return array_map(
            static function (Structure $structure) {
                return $structure->getValue();
            },
            $this->value
        );
    }
}
