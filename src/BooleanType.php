<?php


namespace judahnator\Schema;

use judahnator\Schema\Support\Structure;

final class BooleanType extends Structure
{
    public function __construct(bool $value)
    {
        parent::__construct($value);
    }

    /**
     * Returns the raw value of the given object.
     */
    public function getValue(): bool
    {
        return $this->value;
    }
}
