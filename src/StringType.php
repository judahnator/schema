<?php


namespace judahnator\Schema;

use judahnator\Schema\Support\Structure;

final class StringType extends Structure
{
    public function __construct(string $value)
    {
        parent::__construct($value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
