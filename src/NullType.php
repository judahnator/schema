<?php


namespace judahnator\Schema;

use judahnator\Schema\Support\Structure;

final class NullType extends Structure
{
    public function __construct()
    {
        parent::__construct(null);
    }

    /**
     * Returns the raw value of the given object.
     */
    public function getValue()
    {
        return null;
    }
}
