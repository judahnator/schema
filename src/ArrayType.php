<?php


namespace judahnator\Schema;

use judahnator\Schema\Support\Structure;

final class ArrayType extends Structure
{
    public function __construct(?Structure ...$values)
    {
        // All inputs must be the same type
        if (count($values) > 1) {
            $previous = reset($values);
            while ($current = next($values)) {
                if (!$previous instanceof $current) {
                    throw new \LogicException('All inputs of an array structure must be homogeneous.');
                }
            }
        }

        parent::__construct($values);
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return array_map(
            function (Structure $metadata) {
                return $metadata->getValue();
            },
            $this->value
        );
    }
}
