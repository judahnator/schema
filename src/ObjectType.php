<?php


namespace judahnator\Schema;

use judahnator\Schema\Support\Structure;
use stdClass;

final class ObjectType extends Structure
{
    public function __construct(object $value)
    {
        parent::__construct($value);
    }

    /**
     * @return stdClass
     */
    public function getValue(): stdClass
    {
        $object = new stdClass();
        foreach ($this->value as $k => $v) {
            /** @var Structure $v */
            $object->{$k} = $v->getValue();
        }
        return $object;
    }
}
