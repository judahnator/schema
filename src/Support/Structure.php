<?php


namespace judahnator\Schema\Support;

use JsonSerializable;

abstract class Structure implements JsonSerializable
{
    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * Returns the raw value of the given object.
     */
    abstract public function getValue();

    public function jsonSerialize()
    {
        return $this->getValue();
    }
}
